# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="A terminal locker like vlock"
HOMEPAGE="http://www.math.umd.edu/~sgilles"
EGIT_REPO_URI="https://repo.or.cz/tlock.git"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="alpha amd64 arm hppa ppc ppc64 sparc x86 amd64-fbsd x86-fbsd"
fi

LICENSE="ISC"
SLOT="0"

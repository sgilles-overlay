EAPI=8

inherit git-r3

DESCRIPTION="A set of CLI tools to interact with the Canvas LMS"
HOMEPAGE="http://www.math.umd.edu/~sgilles"
EGIT_REPO_URI="https://repo.or.cz/nci.git"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
fi

LICENSE="ISC"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ppc ppc64 sparc x86 amd64-fbsd x86-fbsd"
RDEPEND="dev-libs/yajl net-misc/curl"

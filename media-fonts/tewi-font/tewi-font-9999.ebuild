EAPI=8

PYTHON_COMPAT=( python{2_7,3_6,3_7,3_8,3_9,3_10} )

inherit font python-any-r1

DESCRIPTION="a bitmap font"
HOMEPAGE="https://github.com/lucy/tewi-font"
if [[ ${PV} != *9999* ]]; then
	SRC_URI="https://github.com/lucy/tewi-font/archive/${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="alpha amd64 arm arm64 hppa ia64 m68k ~mips ppc ppc64 s390 sh sparc x86 ~amd64-fbsd ~sparc-fbsd ~x86-fbsd"
else
	EGIT_REPO_URI="https://github.com/lucy/tewi-font.git"
	inherit git-r3
fi

LICENSE="MIT"
SLOT="0"

IUSE="+pcf bdf"

RDEPEND=""
DEPEND="${PYTHON_DEPS}
	pcf? ( x11-apps/bdftopcf )"

src_compile() {
	if use pcf; then
		emake
	fi

	if use bdf; then
		emake var
	fi
}

src_install() {
	insinto "/usr/share/fonts/${PN}"
	if use pcf; then
		doins out/*
	fi

	if use bdf; then
		doins *.bdf
	fi
	font_src_install
}

# Copyright 1999-2024 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=8
inherit git-r3 savedconfig toolchain-funcs

DESCRIPTION="a generic, highly customizable, and efficient menu for the X Window System"
HOMEPAGE="http://tools.suckless.org/dmenu/"
EGIT_REPO_URI="git://github.com/michaelforney/dmenu.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="savedconfig workingdmenurun"

RDEPEND="
	media-libs/fontconfig
	workingdmenurun? ( x11-misc/dmenu )
"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	x11-libs/pixman
	x11-libs/libxkbcommon
	dev-libs/wld
	media-libs/fontconfig
	dev-libs/wayland
"

src_prepare() {
	sed -i \
		-e 's|^	@|	|g' \
		-e 's|${CC} -o|$(CC) $(CFLAGS) -o|g' \
		-e '/^	echo/d' \
		Makefile || die

	sed -e 's/-Os//g' -i Makefile

	eapply_user

	restore_config config.def.h
}

src_compile() {
	emake CC=$(tc-getCC)

	sed -e 's/dmenu /dmenu-wl /g' -i dmenu_run
	sed -e 's/dmenu/dmenu-wl/g' -i dmenu.1
}

src_install() {
	mv dmenu dmenu-wl
	mv dmenu_run dmenu_run-wl
	mv dmenu.1 dmenu-wl.1
	dobin dmenu-wl
	use workingdmenurun && dobin dmenu_run-wl
	doman dmenu-wl.1

	save_config config.def.h
}

# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

LUA_COMPAT=( lua5-{1..4} luajit )

inherit lua git-r3 toolchain-funcs

DESCRIPTION="Lua bindings for libtermkey"
HOMEPAGE="https://repo.or.cz/lua-termkey.git"
EGIT_REPO_URI="https://repo.or.cz/lua-termkey.git"

if [[ "${PV}" == *999* ]]; then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64"
fi

LICENSE="MIT"
SLOT="0"
IUSE=""

DEPEND="${LUA_DEPS}
	>=dev-libs/libtermkey-0.22"
RDEPEND="${DEPEND}"

DOCS=(README)

src_prepare() {
	default

	lua_copy_sources
}

lua_src_compile() {
	pushd "${BUILD_DIR}" || die

	local myemakeargs=(
		"CC=$(tc-getCC)"
		"LD=$(tc-getCC)"
		"LUAINC_linux=$(lua_get_include_dir)"
		"CFLAGS=${CFLAGS} -I$(lua_get_include_dir)"
		"LDFLAGS=${LDFLAGS}"
	)

	emake "${myemakeargs[@]}"

	popd
}

src_compile() {
	lua_foreach_impl lua_src_compile
}

lua_src_install () {
	pushd "${BUILD_DIR}" || die

	local emakeargs=(
		"INSTALL_CMOD=${ED}/$(lua_get_cmod_dir)"
		"INSTALL_LMOD=${ED}/$(lua_get_lmod_dir)"
	)

	emake "${emakeargs[@]}" install

	popd
}

src_install() {
	lua_foreach_impl lua_src_install

	einstalldocs
}

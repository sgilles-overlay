# Copyright 1999-2024 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

LUA_COMPAT=( lua5-1 lua5-2 lua5-3 lua5-4 luajit )
inherit lua git-r3 autotools toolchain-funcs

DESCRIPTION="Lua Unix Module"
HOMEPAGE="http://github.com/wahern/lunix"
EGIT_REPO_URI="https://github.com/wahern/lunix"

if [[ "${PV}" == *999* ]]; then
	:
else
	EGIT_COMMIT="rel-${PV}"
	KEYWORDS="~amd64 ~arm ~x86"
fi

LICENSE="MIT"
SLOT="0"
IUSE="doc examples"

DEPEND="
	${LUA_DEPS}
	dev-libs/openssl:0=
"
RDEPEND="${DEPEND}"

DOCS=(doc/.)
PATCHES=(
)

lua_src_prepare() {
	if ! test -d "${S}.${ELUA}" ; then
		cp -ral "${S}/" "${S}.${ELUA}" || die
	fi

	cd "${S}.${ELUA}" || die
	eautoheader
	eautoconf
	eautoreconf
}

src_prepare() {
	default
	lua_foreach_impl lua_src_prepare
}

lua_src_configure() {
	cd "${S}.${ELUA}" || die
	default
}

src_configure() {
	lua_foreach_impl lua_src_configure
}


this_compile() {
	cd "${S}.${ELUA}" || die

	local myemakeargs=(
		"CC=$(tc-getCC)"
		"LD=$(tc-getCC)"
		"includedir=$(lua_get_include_dir)"
		"libdir=$($(tc-getPKG_CONFIG) --variable libdir ${ELUA}))"
	)

	if [[ ${ELUA} == "lua5.1" || ${ELUA} == "luajit" ]]; then
		emake all5.1
	fi

	if [[ ${ELUA} == "lua5.2" ]]; then
		emake all5.2
	fi

	if [[ ${ELUA} == "lua5.3" ]]; then
		emake all5.3
	fi

	if [[ ${ELUA} == "lua5.4" ]]; then
		emake all5.4
	fi
}

src_compile() {
	lua_foreach_impl this_compile
}

this_install() {
	# Upstream makefile really, really doesn't respect DESTDIR and has a
	# dozen layers of indirection
	cd "${S}.${ELUA}" || die
	local instdir
	instdir="$(lua_get_cmod_dir)"
	exeinto "${instdir#${EPREFIX}}"
	if [[ ${ELUA} == "lua5.1" || ${ELUA} == "luajit" ]]; then
		doexe src/5.1/unix.so
	fi

	if [[ ${ELUA} == "lua5.2" ]]; then
		doexe src/5.2/unix.so
	fi

	if [[ ${ELUA} == "lua5.3" ]]; then
		doexe src/5.3/unix.so
	fi

	if [[  ${ELUA} == "lua5.4" ]]; then
		doexe src/5.4/unix.so
	fi
}

src_install() {
	lua_foreach_impl this_install
}

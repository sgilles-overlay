# Copyright 1999-2021 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=8

LUA_COMPAT=( lua5-{1..4} luajit )

inherit lua git-r3

DESCRIPTION="Pure Lua implementation of the wcwidth() function"
HOMEPAGE="https://github.com/aperezdc/lua-wcwidth"
EGIT_REPO_URI="https://github.com/aperezdc/lua-wcwidth"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
fi

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~hppa ~ppc ~x86"

DEPEND="${LUA_DEPS}"
RDEPEND="${DEPEND}"

src_prepare() {
	default

	lua_copy_sources
}


lua_src_install() {
	pushd "${BUILD_DIR}" || die

	insinto "$(lua_get_lmod_dir)"
	doins wcwidth.lua
	
	insinto "$(lua_get_lmod_dir)/wcwidth"
	doins wcwidth/init.lua
	doins wcwidth/widetab.lua
	doins wcwidth/zerotab.lua
}

src_install() {
	lua_foreach_impl lua_src_install
}

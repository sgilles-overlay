# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

LUA_COMPAT=( lua5-{1..4} luajit )

inherit lua git-r3 toolchain-funcs

DESCRIPTION="A lua library to spawn programs"
HOMEPAGE="https://github.com/daurnimator/lua-spawn"
EGIT_REPO_URI="https://github.com/daurnimator/lua-spawn"
EGIT_COMMIT="adaa539020e2fae6b17e8b7bb7d20774ba9b0d38"
EGIT_BRANCH="lua-5.4"
KEYWORDS="~amd64 ~arm ~x86"

LICENSE="MIT"
SLOT="0"

PATCHES=( "${FILESDIR}/${P}_makefile.patch" )

DEPEND="
	${LUA_DEPS}
	dev-lua/lunix
"
RDEPEND="
	${DEPEND}
"

src_prepare() {
	default

	lua_copy_sources
}

lua_src_compile() {
	pushd "${BUILD_DIR}" || die

	local myemakeargs=(
		"CC=$(tc-getCC)"
		"LD=$(tc-getCC)"
		"LUAINC_linux=$(lua_get_include_dir)"
		"CFLAGS=${CFLAGS} -I$(lua_get_include_dir)"
		"LDFLAGS=${LDFLAGS}"
	)

	emake "${myemakeargs[@]}"

	popd
}

src_compile() {
	lua_foreach_impl lua_src_compile
}

lua_src_install () {
	pushd "${BUILD_DIR}" || die

	local emakeargs=(
		"INSTALL_CMOD=${ED}/$(lua_get_cmod_dir)"
		"INSTALL_LMOD=${ED}/$(lua_get_lmod_dir)"
	)

	emake "${emakeargs[@]}" install

	popd
}

src_install() {
	lua_foreach_impl lua_src_install

	einstalldocs
}

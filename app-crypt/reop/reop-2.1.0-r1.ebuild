# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs

DESCRIPTION="tedu's Reasonable Expectation Of Privacy"
HOMEPAGE="http://www.tedunangst.com/flak/post/reop"
SRC_URI="http://www.tedunangst.com/flak/files/reop-2.1.0.tgz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ppc ~ppc64 sparc x86 ~amd64-fbsd ~x86-fbsd"

DEPEND="dev-libs/libsodium"
RDEPEND="$DEPEND"

src_prepare() {
	epatch "${FILESDIR}/${P}-respect-DESTDIR.patch"
	epatch "${FILESDIR}/0001-Document-b64_ntop-and-b64_pton.patch"
	eapply_user
}

src_compile() {
	emake CC=$(tc-getCC)
}

# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools git-r3 multilib-minimal

DESCRIPTION="PDF library extracted from TeX's dvipdfmx"
HOMEPAGE="http://sile-typesetter.org"
EGIT_REPO_URI="https://github.com/simoncozens/libtexpdf"

EGIT_COMMIT="50f0d788c358eb6e1953185f91817f885dc19649"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ppc ppc64 sparc x86"

PATCHES=(
	"${FILESDIR}/fix-fs-self-refs.patch"
)

src_prepare(){
	default
	eautoreconf
}

multilib_src_configure() {
	ECONF_SOURCE="${S}" econf
}

multilib_src_install() {
	emake DESTDIR="${D}" install
}

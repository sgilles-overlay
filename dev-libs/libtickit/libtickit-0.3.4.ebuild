# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs git-r3

DESCRIPTION="Terminal Interface Construction Kit"
HOMEPAGE="http://www.leonerd.org.uk/code/libtickit/"
EGIT_REPO_URI="https://github.com/leonerd/libtickit"

LICENSE="MIT"
SLOT="0"
IUSE=""

if [[ "${PV}" == *999* ]]; then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64 ~arm ~x86"
fi

DEPEND="dev-libs/unibilium dev-libs/libtermkey"
RDEPEND=""

PATCHES=(
	"${FILESDIR}/${P}-change-makefile.patch"
)

src_install() {
	emake DESTDIR="${D}" LIBDIR="${EPREFIX}/usr/$(get_libdir)" install

	doman man/*.3
	doman man/*.7

	docinto examples
	dodoc examples/*.c

	find "${D}" -name '*.la' -delete
}

# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="QBE aims to be a pure C embeddable backend."
HOMEPAGE="http://c9x.me/compile/"
EGIT_REPO_URI="git://c9x.me/qbe.git"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="4756643e58965eb21e0bf2ddb45ddb24b9f8bf03"
fi

KEYWORDS="~amd64 ~arm ~x86"
LICENSE="MIT"
SLOT="0"
RDEPEND=""

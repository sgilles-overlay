# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=8

inherit flag-o-matic git-r3 toolchain-funcs

DESCRIPTION="The multifunction open system tool"

HOMEPAGE="https://chromium.googlesource.com/chromiumos/platform/mosys"
SRC_URI=""
EGIT_REPO_URI="https://github.com/dhendrix/mosys"

LICENSE="BSD"
SLOT="0"
KEYWORDS=""

DEPEND="sys-libs/flashmap"

PATCHES=( )

src_configure() {
	emake ARCH=$(tc-arch) defconfig
}

src_compile() {
	emake CC="$(tc-getCC)" LD="$(tc-getLD)" LDFLAGS="$(raw-ldflags)" AR="$(tc-getAR)"
}

src_install() {
	dosbin mosys
}

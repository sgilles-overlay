# Copyright 2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit git-r3

DESCRIPTION="A version of Phil Karn's libfec with some Reed-Solomon decoder bug fixes applied"
HOMEPAGE="https://github.com/fblomqvi/libfec"
EGIT_REPO_URI="https://github.com/fblomqvi/libfec.git"
if [[ ${PV} == 9999 ]]
then
        :
else
        EGIT_COMMIT="c5d935f"
fi

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 x86 amd64-fbsd x86-fbsd"
RDEPEND=""


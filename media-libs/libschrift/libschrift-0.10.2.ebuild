# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit git-r3 multilib-minimal

DESCRIPTION="libschrift is a lightweight TrueType font rendering library."
HOMEPAGE="https://github.com/tomolt/libschrift"
EGIT_REPO_URI="https://github.com/tomolt/libschrift"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="amd64 arm x86"
fi

LICENSE="ISC"
SLOT="0"
RDEPEND="x11-libs/libX11"
DEPEND="${RDEPEND}"

src_prepare() {
	eapply "${FILESDIR}"/0001-add-shared-library-target.patch
	eapply "${FILESDIR}"/0002-add-pkgconfig-file.patch
	eapply "${FILESDIR}"/0003-allow-overrides-to-config.mk.patch
	eapply "${FILESDIR}"/0004-no-demo-or-stress-by-default.patch
	eapply_user

	multilib_copy_sources
	default
}

multilib_src_install() {
	emake DESTDIR="${D}" PREFIX="${EPREFIX}/usr" LIBDIR="${EPREFIX}"/usr/$(get_libdir) install
}

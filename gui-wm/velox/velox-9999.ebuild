# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

EGIT_REPO_URI="git://github.com/michaelforney/velox.git"
KEYWORDS="~amd64 ~arm"
SRC_URI=""

inherit git-r3

DESCRIPTION="velox is a tiling Wayland compositor based on swc"
HOMEPAGE="https://github.com/michaelforney/velox"

LICENCES="MIT"
SLOT="0"
IUSE=""

DEPEND="
	gui-libs/swc
	dev-libs/wld[wayland]
	x11-libs/libxkbcommon
"
RDEPEND="${DEPEND}"

src_configure() {
    tee config.mk << EOF
PREFIX = ${EPREFIX}/usr
LIBDIR = ${EPREFIX}/usr/$(get_libdir)
DATADIR = /usr/share
EOF
}

src_install() {
	emake DESTDIR="${D}" install

        insinto "/usr/share/velox"
	newins velox.conf.sample velox.conf
}

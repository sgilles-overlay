# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="A wrapper to make latexmk more terse."
HOMEPAGE="http://www.math.umd.edu/~sgilles"
EGIT_REPO_URI="https://repo.or.cz/latexmk-quieter.git"
if [[ ${PV} == 9999 ]]
then
	KEYWORDS="~amd64 ~arm ~x86"
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="alpha amd64 arm hppa ppc ppc64 sparc x86 amd64-fbsd x86-fbsd"
fi

LICENSE="ISC"
SLOT="0"
RDEPEND="dev-lua/lua-spawn dev-lua/lunix dev-lua/lpeg"

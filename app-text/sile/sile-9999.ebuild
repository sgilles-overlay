# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

LUA_COMPAT=( lua5-{3..4} )
inherit autotools git-r3 lua-single

DESCRIPTION="Simon's Improved Layout Engine "
HOMEPAGE="http://sile-typesetter.org"
EGIT_REPO_URI="https://github.com/sile-typesetter/sile"
EGIT_SUBMODULES=()

if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
fi

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~hppa ~sparc ~x86"

RDEPEND="
	dev-libs/libtexpdf
	dev-lua/cassowary
	dev-lua/cliargs
	dev-lua/cosmo
	dev-lua/epnf
	dev-lua/linenoise
	dev-lua/lpeg
	dev-lua/luaexpat
	dev-lua/luafilesystem
	dev-lua/luasec
	dev-lua/luasocket
	dev-lua/lua-zlib
	dev-lua/repl
	dev-lua/stdlib
	dev-lua/vstruct
"

src_prepare(){
	default
	rm -rf "${S}/libtexpdf"
	eautoreconf
}

src_configure() {
	ECONF_SOURCE="${S}" econf \
		--with-system-expat \
		--with-system-filesystem \
		--with-system-libtexpdf \
		--with-system-lpeg \
		--with-system-luarocks \
		--with-system-socket \
		--with-system-zlib
}

src_install() {
	emake DESTDIR="${D}" install
}

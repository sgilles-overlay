# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="A toy interface to dev-libs/hyphen"
HOMEPAGE=""
SRC_URI=""

LICENSE="ISC"
SLOT="0"
KEYWORDS="alpha amd64 ~arm ~arm64 hppa ia64 ~m68k ~mips ppc ppc64 ~riscv s390 ~sh sparc x86 ~ppc-aix ~x64-cygwin ~amd64-fbsd ~x86-fbsd ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~m68k-mint ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"

DEPEND="dev-libs/hyphen"

src_unpack() {
	mkdir -p "${P}"
	cp "${FILESDIR}/Makefile" "${P}/Makefile"
	cp "${FILESDIR}/hyshow.c" "${P}/hyshow.c"
}

src_install() {
	dobin hyshow
}

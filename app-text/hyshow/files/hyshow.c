#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wchar.h>
#include <wctype.h>

#include <hyphen.h>

static HyphenDict *
try_load_dict(const char *lang_name)
{
        HyphenDict *ret = 0;
        char *dup = 0;
        size_t len = snprintf(0, 0, "/usr/share/hyphen/hyph_%s.dic", lang_name);

        if (!(dup = malloc(len + 1))) {
                return 0;
        }

        snprintf(dup, len + 1, "/usr/share/hyphen/hyph_%s.dic", lang_name);
        ret = hnj_hyphen_load(dup);
        free(dup);

        return ret;
}

static void
try_hyphenate(HyphenDict *dict, const char *word)
{
        size_t len = strlen(word);
        char *lc = 0;
        char *lcp = 0;
        char *hyphens = 0;
        size_t lclen = 0;
        mbstate_t m = (mbstate_t) { 0 };
        wchar_t wc = 0;
        size_t mbret = 0;
        int spret = 0;
        char **rep = 0;
        int *pos = 0;
        int *cut = 0;

        /* Worst case scenario: a 1 byte upper-case goes to a many byte lower-case */
        if (!(lc = malloc(len * MB_CUR_MAX + 1))) {
                goto done;
        }

        lcp = lc;

        while (len > 0) {
                if ((mbret = mbrtowc(&wc, word, len, &m)) > (size_t) -2) {
                        goto done;
                }

                len -= mbret;
                word += mbret;
                wc = towlower(wc);
                spret = snprintf(lcp, MB_CUR_MAX + 1, "%lc", wc);

                if (spret < 0 ||
                    (size_t) spret >= MB_CUR_MAX) {
                        goto done;
                }

                lcp += spret;
        }

        lclen = strlen(lc);

        if (!(hyphens = malloc(lclen + 5))) {
                goto done;
        }

        hnj_hyphen_hyphenate2(dict, lc, lclen, hyphens, 0, &rep, &pos, &cut);

        for (size_t j = 0; j < lclen; ++j) {
                putchar(lc[j]);

                if (hyphens[j] % 2) {
                        putchar('-');
                }
        }

        printf("\n");
done:
        free(hyphens);
        free(rep);
        free(pos);
        free(cut);
        free(lc);
}

int
main(int argc, char **argv)
{
        char *force_lang = 0;
        char *lc_all = 0;
        char *lang = 0;
        size_t len = 0;
        char *dup = 0;
        char *dot = 0;
        int c = 0;
        HyphenDict *dict = 0;

        setlocale(LC_ALL, "");

        while ((c = getopt(argc, argv, "l:")) != -1) {
                switch (c) {
                case 'l':
                        force_lang = optarg;
                        break;
                default:
                        break;
                }
        }

        if (!dict &&
            force_lang) {
                dict = try_load_dict(force_lang);
                goto dict_should_be_loaded;
        }

        if (!dict) {
                lc_all = getenv("LC_ALL");
                len = strlen(lc_all);

                if ((dup = malloc(len + 1))) {
                        /* This is so I get a damn idiom that works on/off OpenBSD */
                        snprintf(dup, len + 1, "%s", lc_all);

                        if ((dot = strchr(dup, '.'))) {
                                *dot = 0;
                        }

                        dict = try_load_dict(dup);
                }

                free(dup);
        }

        if (!dict) {
                lang = getenv("LANG");
                len = strlen(lang);

                if ((dup = malloc(len + 1))) {
                        snprintf(dup, len + 1, "%s", lang);

                        if ((dot = strchr(dup, '.'))) {
                                *dot = 0;
                        }

                        dict = try_load_dict(dup);
                }

                free(dup);
        }

        if (!dict) {
                dict = try_load_dict("en_US");
        }

dict_should_be_loaded:

        if (!dict) {
                fprintf(stderr,
                        "No suitable hyphenation dictionary found in /usr/share/hyphen/\n");

                return 1;
        }

        for (int j = optind; j < argc; ++j) {
                try_hyphenate(dict, argv[j]);
        }
}

# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 toolchain-funcs

DESCRIPTION="A program to find matroids of plabic graphs"
HOMEPAGE="http://www.math.umd.edu/~sgilles"
EGIT_REPO_URI="git://repo.or.cz/matroid-finder.git"

LICENSE="ISC"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ppc ~ppc64 sparc x86 ~amd64-fbsd ~x86-fbsd"

src_compile() {
	emake CC=$(tc-getCC)
}

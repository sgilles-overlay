# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit flag-o-matic git-r3

DESCRIPTION="Clav is a program to visualize cluster algebras as quivers"
HOMEPAGE="http://www.math.umd.edu/~sgilles"
EGIT_REPO_URI="https://repo.or.cz/clav.git"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="alpha amd64 arm hppa ppc ppc64 sparc x86 amd64-fbsd x86-fbsd"
fi

LICENSE="ISC"
SLOT="0"
IUSE="static"
LIB_DEPEND="
	>=media-libs/sdl2-ttf-2.0.0[static-libs(+)]
	media-libs/libsdl2[static-libs(+)]
	media-fonts/dejavu"
RDEPEND="!static? ( ${LIB_DEPEND//\[static-libs(+)]} )"
DEPEND="${RDEPEND}
	static? ( ${LIB_DEPEND} )"

src_configure() {
	use static && append-ldflags -static
	default
}

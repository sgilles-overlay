EAPI=8

inherit git-r3

DESCRIPTION="Yet another framebuffer terminal"
HOMEPAGE="http://github.com/uobikiemukot/yaft"
EGIT_REPO_URI="git://github.com/uobikiemukot/yaft"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="alpha amd64 arm hppa ppc ppc64 sparc x86 amd64-fbsd x86-fbsd"
fi

LICENSE="MIT"
SLOT="0"
DEPEND="media-fonts/tewi-font[bdf]"

PATCHES=(
	"${FILESDIR}/0001-Support-24bit-color-escape-sequences.patch"
	"${FILESDIR}/0002-Use-tewi-font.patch"
	"${FILESDIR}/0003-Allow-partial-redraws-of-lines.patch"
)

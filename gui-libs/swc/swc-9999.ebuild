# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

EGIT_REPO_URI="git://github.com/michaelforney/swc.git"
KEYWORDS="~amd64 ~arm"
SRC_URI=""

inherit git-r3

DESCRIPTION="swc is a library for making a simple Wayland compositor"
HOMEPAGE="https://github.com/michaelforney/swc"

LICENCE="MIT"
SLOT="0"
IUSE="udev xwayland"

# The Exherbo file includes dependency on eudev/systemd as required. I
# can't test that.
DEPEND="
	dev-libs/libevdev
	dev-libs/libinput
	dev-libs/wld[drm]
	dev-libs/wayland
	dev-libs/wayland-protocols
	x11-libs/libdrm
	x11-libs/libxkbcommon
	x11-libs/pixman
	udev? (
		virtual/udev
	)
	xwayland? (
		x11-libs/libxcb
		x11-utils/xcb-util-wm
	)
"
RDEPEND="${DEPEND} xwayland? ( x11-server/xorg-server[xwayland] )"

src_configure() {
	cat > config.mk << EOF
PREFIX              = ${EPREFIX}/usr
LIBDIR              = ${EPREFIX}/usr/$(get_libdir)
DATADIR             = ${EPREFIX}/usr/share
ENABLE_DEBUG        = 1
ENABLE_STATIC       = 1
ENABLE_SHARED       = 1
ENABLE_LIBUDEV      = $(usex udev 1 0)
ENABLE_XWAYLAND     = $(usex xwayland 1 0)
EOF

	cat config.mk
}

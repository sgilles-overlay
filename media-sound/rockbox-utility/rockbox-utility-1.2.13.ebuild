# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

DESCRIPTION="Graphical utility for automating Rockbox firmware installation and management"
HOMEPAGE="http://www.rockbox.org/wiki/RockboxUtility"
SRC_URI="http://download.rockbox.org/rbutil/source/RockboxUtility-v${PV}-src.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""
RESTRICT=""

RDEPEND="!media-sound/rockbox-utility-bin
	dev-libs/libusb"
DEPEND="x11-libs/qt-core
	x11-libs/qt-gui[accessibility]
	${RDEPEND}"

S="${WORKDIR}/RockboxUtility-v1.2.13"

src_compile() {
	      mkdir build
	      cd build
	      eqmake4 ../rbutil/rbutilqt/rbutilqt.pro || "die qmake failed"
	      emake || "die emake failed"
}

src_install() {
	      dobin build/RockboxUtility || die "dobin failed"
	      dodoc docs/CREDITS
}
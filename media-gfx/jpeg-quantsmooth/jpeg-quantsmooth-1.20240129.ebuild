# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="jpeg-quantsmooth tries to recreate lost precision of DCT coefficients based on quantization table from jpeg image."
HOMEPAGE="https://github.com/ilyakurdyukov/jpeg-quantsmooth"
EGIT_REPO_URI="https://github.com/ilyakurdyukov/jpeg-quantsmooth"

LICENSE="LGPL-2.1"
SLOT="0"

if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="${PV}"
	KEYWORDS="~alpha amd64 arm arm64 hppa ~ia64 ~m68k ~mips ~ppc ppc64 ~s390 sparc x86 ~x64-cygwin ~amd64-linux ~x86-linux ~x64-macos ~x86-macos"
fi

IUSE=""
RDEPEND="
	media-libs/libjpeg-turbo
"
DEPEND="${RDEPEND}"

src_install() {
	dobin jpegqs
}

# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="Tiny image viewer for framebuffer"
HOMEPAGE="http://github.com/uobikiemukot/idump"
EGIT_REPO_URI="git://github.com/uobikiemukot/idump"
if [[ ${PV} == 9999 ]]
then
	:
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="alpha amd64 arm hppa ppc ppc64 sparc x86 amd64-fbsd x86-fbsd"
fi

LICENSE="MIT"
SLOT="0"
DEPEND="media-libs/libpng virtual/jpeg"

PATCHES=(
	"${FILESDIR}/0001-Make-Makefile-portable.patch"
)

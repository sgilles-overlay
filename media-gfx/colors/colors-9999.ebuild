# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 savedconfig

DESCRIPTION="A simple program to extract colors from PNG files"
HOMEPAGE="https://git.2f30.org/colors/log.html"
EGIT_REPO_URI="git://git.2f30.org/colors.git"

LICENSE="ISC"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ppc ppc64 sparc x86 amd64-fbsd x86-fbsd"

IUSE=""

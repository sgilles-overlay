# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
EAPI=5

DESCRIPTION='Empty package, containing only dependencies for wakarimasen'
LICENSE=" GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
DEPEND="
	>=dev-python/jinja-2.0.0
	dev-python/werkzeug
	dev-python/sqlalchemy
	media-gfx/imagemagick
	www-servers/uwsgi
"

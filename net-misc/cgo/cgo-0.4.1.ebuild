# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 toolchain-funcs

DESCRIPTION="A terminal based gopher client"
HOMEPAGE="http://github.com/kieselsteini/cgo"
EGIT_REPO_URI="git://github.com/kieselsteini/cgo"
EGIT_COMMIT="9b021ca22670353f864e49e17eef2dfb9d8fb019"

LICENSE="ISC"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ppc ppc64 sparc x86 amd64-fbsd x86-fbsd"

src_prepare() {
	epatch "${FILESDIR}/${P}--Fix-crash-on-attempting-to-download-telnet-sessions.patch"
	epatch "${FILESDIR}/${P}--More-explicit-error-on-getaddrinfo-failure.patch"

	default
}

src_compile() {
	emake CC=$(tc-getCC)
}

src_install() {
	dobin cgo
	doman cgo.1

	insinto /etc
	doins cgorc
}

# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 savedconfig

DESCRIPTION="sacc(omys), simple console gopher client"
HOMEPAGE="gopher://bitreich.org/scm/sacc"
EGIT_REPO_URI="git://bitreich.org/sacc"

LICENSE="ISC"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ppc ppc64 sparc x86 amd64-fbsd x86-fbsd"

IUSE="ncurses"

RDEPEND="dev-libs/libretls"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

src_prepare() {
	restore_config config.h
	default

	echo '' > config.mk
	echo "PREFIX = ${EPREFIX}/usr"                  >> config.mk

	if use ncurses; then
		echo 'UI=ti'                            >> config.mk
		echo 'LIBS=-lncurses -ltinfo'           >> config.mk
	else
		echo 'UI=text'                          >> config.mk
	fi

	echo 'OSCFLAGS = -D_DEFAULT_SOURCE $(CFLAGS)' >> config.mk
	echo 'OSCFLAGS += -D_XOPEN_SOURCE=700'        >> config.mk
	echo 'OSCFLAGS += -D_BSD_SOURCE'              >> config.mk
	echo 'OSCFLAGS += -D_GNU_SOURCE'              >> config.mk
	echo 'OSCFLAGS += $(CFLAGS)'                  >> config.mk
	echo ''                                       >> config.mk
	echo 'IO = tls'                               >> config.mk
	echo 'IOLIBS = -ltls'                         >> config.mk
	echo 'IOCFLAGS = -DUSE_TLS'                   >> config.mk
}

src_install() {
	save_config config.h
	dobin sacc
	doman sacc.1
}

# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="simple off-line blog utility"
HOMEPAGE="http://kristaps.bsd.lv/sblg/"
SRC_URI="http://kristaps.bsd.lv/sblg/snapshots/${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ppc ppc64 sparc x86 amd64-fbsd x86-fbsd"
RDEPEND="dev-libs/expat"
DEPEND="${RDEPEND}"

src_prepare() {
    sed -e 's/\/usr\/local/\/usr/' -i Makefile
    sed -e 's/$(PREFIX)\/man/$(PREFIX)\/share\/man/' -i Makefile

    default
}